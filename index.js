const express = require('express');
const app = express();
var cookieParser = require('cookie-parser')

// Logging
const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
const loggingBunyan = new LoggingBunyan();
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/bunyan_log"
const logger = bunyan.createLogger({
  // The JSON payload of the log as it appears in Cloud Logging
  // will contain "name": "my-service"
  name: 'recipe-website-269020',
  streams: [
    // Log to the console at 'info' and above
    {stream: process.stdout, level: 'info'},
    // And log to Cloud Logging, logging at 'info' and above
    loggingBunyan.stream('info'),
  ],
});


app.use(express.static('public'));
app.use(express.urlencoded({
	extended: false
}));
app.use(express.json());
app.use(cookieParser());
app.set('view engine', 'pug');
app.set('views', './views');
app.listen(8080);

// app.use (function (req, res, next) {
//     if (req.secure) {
//         // request was via https, so do no special handling
//         next();
//     } else {
//         // request was via http, so redirect to https
//         res.redirect('https://' + req.headers.host + req.url);
//     }
// });

require('./local_modules/routes') (app, logger);
require('./local_modules/uploads') (app);
require('./local_modules/requests') (app);

/* ------------------------ 404 ----------------------- */
app.get('*', function(req, res) {
    logger.info('Site request - 404: ' + req.originalUrl);
    res.status(404);
    res.render('notfound', {name: '404 Not Found'});
});