class DBMessageService {
    
    constructor () {
    
    }
    
    static getRecipeData (recipeName) {
      return DBMessageService._submitGetRequest('/request/recipe/' + recipeName);
    }

    static getSections () {
      return DBMessageService._submitGetRequest('/request/sections');
    }

    static getSectionRecipes (sectionName) {
      return DBMessageService._submitGetRequest('/request/recipes/' + sectionName);
    }

    static getAllRecipes () {
      return DBMessageService._submitGetRequest('/request/allrecipes');
    }

    static getAllRecipeNames () {
      return DBMessageService._submitGetRequest('/request/allrecipenames');
    }

    static getRecipesFromSearch (searchText) {
      return DBMessageService._submitGetRequest('/request/recipeSearch/' + searchText);
    }

    static getPaginatedRecipes (startAfter) {
      return DBMessageService._submitGetRequest('/request/paginatedrecipes/' + startAfter);
    }

    static getRandomRecipe () {
      return DBMessageService._submitGetRequest('/request/randomrecipe');
    }

    static getRecipesWithTag (tagName) {
      return DBMessageService._submitGetRequest('/request/tag/' + tagName);
    }

    static sendLoggingData (dataString) {
      return DBMessageService._submitPostRequest('/log/' + dataString);
    }

    static _submitGetRequest (channel) {
      return new Promise ( function (resolve, reject) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                resolve(xmlHttp.responseText);
            }
        }
        xmlHttp.open("GET", channel, true); // true for asynchronous 
        xmlHttp.send(null);
      });
    }

    static _submitPostRequest (channel) {
      return new Promise ( function (resolve, reject) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                resolve(xmlHttp.responseText);
            }
        }
        xmlHttp.open("POST", channel, true); // true for asynchronous 
        xmlHttp.send(null);
      });
    }
};

function getFakeData (recipeName) {
return {
          name: "Carrot Cake Muffins",
          section: "Dessert",
          servings: 10,
          image: "/images/carrotCakeMuffins.jpeg",
          duration: "3 hours",
          ingredients: [
            "2 Tbsp sugar",
            "1 egg",
            "1/3 cup milk",
            "1 cup salt",
            "3 diabetes"
          ],
          subIngredients: {
            "Sauce": [
              "1 Tsp water",
              "3 cups other",
              "Splash of vodka",
              "Pinch of salt"
            ],
            "Filling": [
              "6 cups butters",
              "13 shots of tequila"
            ]
          },
          steps: [
            "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
            "Shake that booty",
            "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident",
            "Eat yo' shortbread"
          ],
          tags: [
            "Dessert",
            "Breakfast",
          ],
          uploader: "Brittany"
        };
        }