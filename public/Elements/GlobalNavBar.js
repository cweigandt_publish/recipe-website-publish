class GlobalNavBar extends AbstractCustomElement {

    constructor () {
        super();
    }
            
    connectedCallback () {
        super.connectedCallback();
        DBMessageService.getSections().then(function (data) {
            this._sections = JSON.parse(data).data;

            // Highlight the active page in navbar
            if (/sections\/*/.test(window.location.pathname)) {
                var lastPart = window.location.pathname.split("/").pop();
                this._activeSection = lastPart;
            } else if (/\//.test(window.location.pathname)) {
                this._activeSection = "Home";
            }

            resetBrandName();
            
            this._createListItem("Home", "/", this._activeSection === "Home");
            this._sections.sort().forEach(section => {
                this._createListItem(section, "/sections/" + section, this._activeSection === section);
            }, this);
        }.bind(this));

        this.initializeSettings();
    }
    
    _getTemplate() {
        const navbarTheme = JSON.parse(this.getAttribute('useDarkMode')) ? 'navbar-dark' : 'navbar-light';
        return `
            <nav id="navbarParent" class="noprint custom-navbar fixed-top ` + navbarTheme + `">

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a class="navbar-brand" id="navBarBrand" href="/"></a>

                <div class="navbar-collapse collapse" id="navbarNav">
                    <ul class="nav navbar-nav" id="navbarListHolder">

                    </ul>
                </div>

                <div>
                    <button onclick="location.href = '/grid';" class="navbar-toggler settings-toggle float-xs-right" type="button">
                        <i class="fa fa-th-large" style="background-color: transparent;"></i>
                    </button>
                    <button class="navbar-toggler collapsed settings-toggle float-xs-right" type="button" data-toggle="collapse" data-target="#settingsPanel" aria-controls="settingsPanel">
                        <i class="fa fa-gear"></i>
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="settingsPanel">
                    <div class="settings-row">
                        <input type="checkbox" checked 
                            id="darkModeToggle"
                            data-toggle="toggle" 
                            data-style="ios" 
                            data-on="Light" 
                            data-off="Dark"
                            data-size="sm"
                            data-onstyle="light" data-offstyle="dark" data-style="border">
                        <span>Color Theme:</span>
                    </div>
                </div>
            </nav>
        `;
    }
    
    _createListItem (title, url, isActive) {
        var item = document.createElement("li");
        item.classList = "nav-item" + (isActive ? " active" : "");
        
        var link = document.createElement("a");
        link.classList = "nav-link";
        link.href = url;
        link.innerHTML = title;
        item.appendChild(link);
    
        document.getElementById("navbarListHolder").appendChild(item);
    }

    initializeSettings () {
        this._listenToAndUpdateToggle();
    }

    _listenToAndUpdateToggle () {
        let toggle = document.getElementById("darkModeToggle");
        if (isDarkModeEnabled()) {
            toggle.checked = false;
        }

        toggle.onchange = function() {
            if (this.checked) {
                changeDarkMode(false);
            } else {
                changeDarkMode(true);
            }
        };
    }

};
customElements.define('global-nav-bar', GlobalNavBar);

function setBrandName (text) {
    const navBarBrandDiv = document.getElementById('navBarBrand');
    navBarBrandDiv.innerHTML = text;
}

function resetBrandName () {
    const defaultBrandName = "B+C Cookbook";
    const navBarBrandDiv = document.getElementById('navBarBrand');
    navBarBrandDiv.innerHTML = defaultBrandName;
}