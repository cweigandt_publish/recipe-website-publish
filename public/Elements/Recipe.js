class Recipe extends AbstractCustomElement {

    constructor () {
        super();
    }
            
    connectedCallback () {
        super.connectedCallback();
        
        this._listenToScroll();

        this._recipe = JSON.parse(this.getAttribute('recipe'));
        document.getElementById("recipeTitle").innerHTML = this._recipe.name;
        document.getElementById("sectionName").innerHTML = this._recipe.section.toLowerCase();
        document.getElementById("servingsNumber").innerHTML += this._recipe.servings;

        // Start location at /uploads/img/... instead of /public/uploads/img/...
        var imageSrc = this._recipe.imageLocation;
        document.getElementById("recipeImage").src = imageSrc;

        // Generate and add ingredients divs to left
        this._recipe.ingredients.forEach(function (ingredient) {
            this._appendIngredientListItem(ingredient);
        }.bind(this));

        // Generate and add any sub ingredient headers and lists (e.g. sauce)
        if (this._recipe.subIngredients1Name !== "") {
            this._appendSubIngredientsList(this._recipe.subIngredients1Name, this._recipe.subIngredients1);
        }

        if (this._recipe.subIngredients2Name !== "") {
            this._appendSubIngredientsList(this._recipe.subIngredients2Name, this._recipe.subIngredients2);
        }

        // Generate tags
        this._appendTags(this._recipe.tags);

        // Generate and add steps divs
        this._recipe.steps.forEach(function (step, i) {
            this._appendRecipeStepListItem(step, i + 1);
        }.bind(this));
        
        setTimeout ( function () {
            document.getElementById("recipeWrapper").classList.add("loaded");
            document.getElementById("recipeWrapper").style.opacity = 1;
        }, 300);

        this._showEditIfNeeded();
    }
    
    _getTemplate() {
            return `
            <div id="recipeWrapper" class="print" style="opacity: 0">
                <div id="recipeTitle"></div>
                <div id="subTitle">
                    <span id="sectionName"></span>
                    <span id="dotSeparator">&#9679;</span>
                    <span id="servings">servings | </span><span id="servingsNumber"></span>
                </div>
                <div id="socialButtons" class="noprint">
                    <button class="btn socialIcon" onclick="window.print(); return false;"><i class="fa fa-print"></i></button>
                </div>
                
                <div class="image-wrapper">
                    <img id="recipeImage" src=""/>
                </div>
            
                <div class="row no-gutters" id="recipeBody">
                    <div class="col-sm-4" id="ingredientsColumn">
                        <div class="sticky" id="ingredientsColumnSticky">
                        <div class="ingredients-title">Ingredients</div>
                        <ul class="ingredients-list" id="ingredientsList">
                        
                        </ul>
                        </div>
                    </div>
                    <div class="col-sm-8" id="stepsColumn">
                        <ul id="stepsList">
                        
                        </ul>
                    </div>
                </div>
            </div>
            `;
    }
    
    _appendIngredientListItem (ingredient) {
        var item = document.createElement("li");
        item.classList = "ingredient-item";
        item.innerHTML = ingredient;

        document.getElementById("ingredientsList").appendChild(item);
    }
    
    _appendSubIngredientsList (title, ingredientList) {
        var listTitle = document.createElement("div");
        listTitle.classList = "ingredients-title";
        listTitle.innerHTML = title;
        
        var list = document.createElement("ul");
        list.classList = "ingredients-list";
        
        ingredientList.forEach(function (ingredient) {
            var item = document.createElement("li");
            item.classList = "ingredient-item";
            item.innerHTML = ingredient;
            list.appendChild(item);
        });
        
        document.getElementById("ingredientsColumnSticky").appendChild(listTitle);
        document.getElementById("ingredientsColumnSticky").appendChild(list);
    }
    
    _appendRecipeStepListItem (step, stepNumber) {
        var item = document.createElement("li");
        item.classList = "recipe-step-item";
        
        var stepTitle = document.createElement("div");
        stepTitle.classList = "stepTitle";
        stepTitle.innerHTML = "Step " + stepNumber;
        item.appendChild(stepTitle);
        
        var stepText = document.createElement("div");
        stepText.classList = "stepText";
        stepText.innerHTML = step;
        item.appendChild(stepText);
    
        document.getElementById("stepsList").appendChild(item);
    }
    
    _appendTags (tags) {
        if (tags.length === 0) {
            // Don't add tags title or divs if there are none
            return;
        }

        var wrapper = document.createElement("div");
        wrapper.classList = "tag-wrapper noprint";
        
        var tagTitle = document.createElement("div");
        tagTitle.classList = "tagTitle";
        tagTitle.innerHTML = "Tags";
        wrapper.appendChild(tagTitle);
        
        tags.forEach(function (tag) {
            var tagDiv = this._createTag(tag);
            wrapper.appendChild(tagDiv);
        }.bind(this));
        
        // Append to both ingredients and steps column as responsive CSS will determine which to show
        document.getElementById("ingredientsColumnSticky").appendChild(wrapper);

        var wrapper2 = wrapper.cloneNode(true);
        document.getElementById("stepsColumn").appendChild(wrapper2);
    }
    
    _createTag (text) {
        var tag = document.createElement("div");
        tag.classList = "badge badge-primary";
        
        var link = document.createElement("a");
        link.classList = "tag-link";
        link.href = "/tag/" + text.replace(/ /g, "_");
        link.innerHTML = text;

        tag.appendChild(link);

        return tag;
    }

    _showEditIfNeeded () {
        if (canEditFromRecipe()) {
            const socialWrapper = document.getElementById('socialButtons');

            const editButton = document.createElement("button");
            editButton.classList = "btn socialIcon";
            editButton.innerHTML = '<i class="fa fa-edit"></i>';
            editButton.onclick = () => {
                // Make a fake form so we can send post data with the redirect
                var form = document.createElement("form");
                form.method = 'get';
                form.action = '/edit';
                var input = document.createElement('input');
                input.type = "text";
                input.name = "recipeName";
                input.value = this._recipe.name.replace(/ /g, '_');
                form.appendChild(input);

                document.body.appendChild(form);
                form.submit();
            }
            socialWrapper.appendChild(editButton);


        }
    }
    
    _listenToScroll () {
        window.addEventListener('scroll', function() {  
            if (window.scrollY > 50 && this._wasScrollLessThan50) {
                setBrandName(this._recipe.name)
                this._wasScrollLessThan50 = false;
            } else if (window.scrollY <= 50 && !this._wasScrollLessThan50) {
                resetBrandName();
                this._wasScrollLessThan50 = true;
            }
        }.bind(this));
    }
};
customElements.define('recipe-element', Recipe);