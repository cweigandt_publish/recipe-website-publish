class AbstractCustomElement extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback () {
        this.innerHTML = this._getTemplate();
    }
};