class RecipePickerFormPopulater extends AbstractCustomElement {

    constructor () {
        super();

        this._ids = [
            'nameInput',
            'sectionInput',
            'servingsInput',
            'timeInput',
            'imageInput',
            'ingredientsInput',
            'subIngredients1Name',
            'subIngredients1Input',
            'subIngredients2Name',
            'subIngredients2Input',
            'stepsInput',
            'tagsInput',
            'uploaderInput',
            'submitInput'
        ];
    }
            
    connectedCallback () {
        super.connectedCallback();

        this._initialRecipe = this.getAttribute('initialRecipe');

        DBMessageService.getAllRecipeNames().then(function (data) {
            var recipeNames = JSON.parse(data);
            this._buildDropdown(recipeNames);
        }.bind(this));
    }

    _buildDropdown (recipeNames) {
        var select = document.getElementById("recipePicker");
        var option = this._buildOption("");
        select.appendChild(option);

        recipeNames.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            else if (a.name > b.name) {
                return 1;
            }
            return 0;
        });

        recipeNames.forEach(function (recipe) {
            var option = this._buildOption(recipe.name);
            select.appendChild(option);
        }, this)
        select.onchange = this._handleDropdownChanged.bind(this);
        this._setFormEnabled(false);

        if (this._initialRecipe) {
            const initialRecipe = this._initialRecipe.replace(/_/g, ' ');
            const targetOption = document.querySelector('#recipePicker [value="' + initialRecipe + '"]');
            if (targetOption) {
                targetOption.selected = true;
                this._handleDropdownChanged({target: targetOption});
            }
            else {
                alert('Could not find recipe ' + initialRecipe);
            }
        }
    }

    _buildOption (name) {
        var option = document.createElement("option");
        option.value = name;
        option.innerHTML = name;
        return option;
    }
    
    _getTemplate () {
        return `
            <select id="recipePicker">
            </select>
        `;
    }

    _handleDropdownChanged (ev) {
        var recipeName = ev.target.value;
        
        DBMessageService.getRecipeData(recipeName)
        .then(recipe => {
            const parsedRecipe = JSON.parse(recipe);
            if (!parsedRecipe) {
                // Empty element was chosen
                this._setFormEnabled(false);
                return;
            }
            this._populateForm(parsedRecipe);
            this._setFormEnabled(true);        
        });
        
        return true;
    }

    _populateForm (recipe) {
        this._setField('nameInput', recipe.name);
        this._setField('sectionInput', recipe.section);
        this._setField('servingsInput', recipe.servings);
        this._setField('timeInput', recipe.time);

        this._setField('ingredientsInput', recipe.ingredients.join('\n'));
        this._setField('subIngredients1Name', recipe.subIngredients1Name);
        this._setField('subIngredients1Input', recipe.subIngredients1.join('\n'));
        this._setField('subIngredients2Name', recipe.subIngredients2Name);
        this._setField('subIngredients2Input', recipe.subIngredients2.join('\n'));

        this._setField('stepsInput', recipe.steps.join('\n'));
        this._setField('tagsInput', recipe.tags.join(', '));
        this._setField('uploaderInput', recipe.uploader);

        var formWrapper = document.getElementById('formWrapper');
        formWrapper.action = '/editRecipe';
    }

    _setField (id, value) {
        var field = document.getElementById(id);
        field.value = value;
    }

    _setFormEnabled (enable) {
        this._ids.forEach(function (id) {
            var input = document.getElementById(id);
            input.disabled = !enable;
            if (!enable) {
                input.value = "";
            }
        })
    }
};
customElements.define('recipe-picker-form-populater', RecipePickerFormPopulater);