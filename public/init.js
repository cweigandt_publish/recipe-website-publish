
(function requestLocation () {
  var request = new XMLHttpRequest();

  request.onload = function (data) {
    const response = JSON.parse(request.response);
    DBMessageService.sendLoggingData('Location: ' + response.geoplugin_city + ', ' + response.geoplugin_region + ', ' + response.geoplugin_countryName)
  };
  request.open('get', 'http://www.geoplugin.net/json.gp', true);
  request.send();
}());

function applyDarkModeChanges () {
    if (isDarkModeEnabled()) {
        document.body.classList.add("dark-theme");
        document.getElementById("navbarParent").classList.remove("navbar-light");
        document.getElementById("navbarParent").classList.add("navbar-dark");
    } else {
        document.body.classList.remove("dark-theme");
        document.getElementById("navbarParent").classList.add("navbar-light");
        document.getElementById("navbarParent").classList.remove("navbar-dark");
    }
}

function changeDarkMode (isDarkModeEnabled) {
    setCookie('bccookbook-dark-mode-enabled', isDarkModeEnabled, 1000);
    applyDarkModeChanges();
}

function isDarkModeEnabled () {
    return getCookie('bccookbook-dark-mode-enabled') === 'true';
}

function setShowEditCookie () {
    setCookie('bccookbook-can-edit', true, 1000);
}

function canEditFromRecipe () {
    return getCookie('bccookbook-can-edit') === 'true';
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;SameSite=Lax;";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function debounce(func, timeout = 300){
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}