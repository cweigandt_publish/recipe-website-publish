const customDB = require('./db');

module.exports = function (app) {

    app.get('/request/recipe/:recipeName', (req, res) => {
        var requestedRecipe = req.params.recipeName;
        console.log("Requesting recipe " + requestedRecipe);

        requestedRecipe = requestedRecipe.replace(/_/g, " ");
        var dbPromise = customDB.requestRecipe(requestedRecipe);
        dbPromise.then(
            recipeData => {
                res.send(recipeData);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/sections', (req, res) => {
        var dbPromise = customDB.getSections();
        dbPromise.then(
            sections => {
                res.send(sections);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/allrecipes', (req, res) => {
        var dbPromise = customDB.getAllRecipes();
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/paginatedrecipes/:startAfter', (req, res) => {
        const startAfter = req.params.startAfter;

        var dbPromise = customDB.getPaginatedRecipes(startAfter);
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/recipeSearch/:searchText', (req, res) => {
        var dbPromise = customDB.getAllRecipes(true);
        dbPromise.then(
            recipes => {
                const matchedRecipes = [];
                recipes.forEach(recipe => {
                    if (recipe.name.toUpperCase().match(req.params.searchText.toUpperCase()) !== null) {
                        matchedRecipes.push(recipe);
                    }
                });
                res.send(matchedRecipes);

            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/randomrecipe', (req, res) => {
        var dbPromise = customDB.getRandomRecipe();
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/numrecipes', (req, res) => {
        var dbPromise = customDB.getNumberOfRecipes();
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/allrecipenames', (req, res) => {
        var dbPromise = customDB.getNamesOfRecipes();
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/recipes/:sectionName', (req, res) => {
        var sectionName = req.params.sectionName;

        var dbPromise = customDB.getSectionRecipes(sectionName);
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/request/tag/:tagName', (req, res) => {
        var tagName = req.params.tagName;

        var dbPromise = customDB.getRecipesWithTag(tagName);
        dbPromise.then(
            recipes => {
                res.send(recipes);
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });
};