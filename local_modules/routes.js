const customDB = require('./db');
const pug = require('pug');

module.exports = function (app, logger) {

    const cachedCompile = pug.compileFile('./views/carddeck.pug', {cache: true});

    app.use(function(req, res, next) {
        if (!req.path.startsWith('/request') && !req.path.startsWith('/log')) {
            // Ignore any requests and only log sites visited for now
            logger.info('Site request - ' + req.originalUrl);
        }
        next();
    });

    // Cookie passing
    app.use(function (req, res, next) {
        if (!req.path.startsWith('/request')) {
            const cookies = req.cookies;
            // Pass info to next handler
            req.useDarkMode = cookies['bccookbook-dark-mode-enabled'] == 'true' || false;
        }
        next();
    });

    app.get('/', (req, res) => {
        customDB.getAllRecipes().then(
            recipeData => {
                res.send(cachedCompile({
                    name: 'B+C Cookbook',
                    useDarkMode: req.useDarkMode,
                    recipes: recipeData,
                    isPaginated: true,
                    cache: true
                }));
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/recipe/:recipeName', (req, res) => {
        var requestedRecipe = req.params.recipeName.replace(/_/g, ' ');
        var dbPromise = customDB.requestRecipe(requestedRecipe);
        dbPromise.then(
            recipeData => {
                res.render('recipe', {
                    name: requestedRecipe,
                    useDarkMode: req.useDarkMode, 
                    recipeName: requestedRecipe,
                    image: recipeData.imageLocation,
                    recipe: recipeData
                });
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/sections/[a-zA-Z]+', (req, res) => {
        var requestedSection = req.originalUrl.split('/').pop();
        var dbPromise = customDB.getSectionRecipes(requestedSection);
        dbPromise.then(
            recipeData => {
                res.render('carddeck', {
                    name: requestedSection,
                    useDarkMode: req.useDarkMode, 
                    recipes: recipeData,
                    isPaginated: true
                });
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/tag/[a-zA-Z_\']+', (req, res) => {
        var requestedTag = req.originalUrl.split('/').pop();
        requestedTag = requestedTag.replace(/_/g, ' ');
        var dbPromise = customDB.getRecipesWithTag(requestedTag);
        dbPromise.then(
            recipeData => {
                res.render('carddeck', {
                    name: requestedTag,
                    useDarkMode: req.useDarkMode, 
                    recipes: recipeData,
                    isPaginated: true
                });
            }
        ).catch(
            err => {
                res.status(500);
                console.log(err);
            }
        );
    });

    app.get('/login', (req, res) => {
        res.render('login', {
            name: 'Log In',
            useDarkMode: req.useDarkMode
        });
    });

    app.get('/grid', (req, res) => {
        var dbPromise = customDB.getRecipeImages();
        dbPromise.then(
            recipeData => {
                res.render('grid', {
                    name: 'Grid',
                    recipes: recipeData,
                    useDarkMode: req.useDarkMode
                });
            });
    });

    app.get('/upload', (req, res) => {
        res.render('upload', {
            name: 'Upload Recipe',
            useDarkMode: req.useDarkMode
        });
    });

    app.get('/edit', (req, res) => {
        res.render('edit', {
            name: 'Edit Recipe',
            useDarkMode: req.useDarkMode,
            initialRecipe: req.query.recipeName
        });
    });

    app.post('/log/:data', (req, res) => {
        logger.info(req.params.data);
        res.status(200);
        res.send('Success');
    });
};